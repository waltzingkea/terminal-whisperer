# Terminal Whisperer

terminal-whisperer is a bash script that sends the results of terminal commands to a specified Telegram chat via a bot.

### Installing
- Put the `terminal-whisperer` somewhere in your `$PATH`
- Create a telegram bot and get the chat ID and bot Token, search online for how to do this.
- Edit `terminal-whisperer` to use your telegram bot chat ID and bot token. 

## Sending Command Results

Here is an example to send the message "Hello, world!" along with the exit status of the previous command to the Telegram bot.
```bash
echo "Hello, world!" | terminal-whisperer "test command sending output"
```

Here is an example not piping the result from the previous command.
```bash
echo "Hello, world!"; terminal-whisperer "test command without output"
```


## Advertising Material
Calling all coders, scripters, and terminal-jockeys! Have you ever kicked off a script that chugs away, day in and day out, like a faithful old steam engine? Ever had a batch job that's been running so long you've forgotten what it was even supposed to do? Ever wished there was a way to know when your long-running task has finally crossed the finish line?

Say goodbye to endless waiting, constant checking, and nagging uncertainty! Presenting 🥁🥁🥁... the **Terminal Whisperer**! 🎉🎉🎉

This app is your personal sentinel, your watchful guardian, your loyal messenger. It keeps a keen eye on your scripts, and the moment they're done, BAM! You get a notification right in your Telegram chat. It doesn't matter if the script concluded in a triumphant success or a heartbreaking failure, Terminal Whisperer will be there to let you know.

But that's not all! This wonderful app also delivers the final output of your script straight to your chat, so you can see the fruits of your script's labor without even touching the terminal. Perfect for those who are always on the go!

No more suspense, no more waiting, no more uncertainty! With Terminal Whisperer, you can kick back, relax, and let your scripts do their thing, knowing that you'll be the first to know when they're done. 

So get the Terminal Whisperer now, and turn your terminal into a whispering companion! 🚀🚀🚀"

(Note: The Terminal Whisperer is not responsible for scripts that run indefinitely, consume all your system resources, or summon elder gods from the depths of the Internet. Always remember to code responsibly!)